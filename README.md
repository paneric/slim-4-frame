# Slim 4 Skeleton 

Simple skeleton/boilerplate that integrates base development packages: 

*  [PHP-DI](http://php-di.org/doc/)
*  Doctrine:
    *  [ORM](https://www.doctrine-project.org/projects/doctrine-orm/en/2.6/index.html)
    *  [Migrations](https://www.doctrine-project.org/projects/doctrine-migrations/en/2.1/index.html)
*  Symfony Components:
    *  [Form](https://symfony.com/doc/current/components/form.html#installation)
    *  [CSRF Protection](https://symfony.com/doc/current/security/csrf.html)
    *  [Translation](https://symfony.com/doc/current/translation.html#installation)
    *  [Validation](https://symfony.com/doc/current/validation.html)
    *  [HttpFoundation](https://symfony.com/doc/current/create_framework/http_foundation.html)
* Twig ([Slim Twig View](https://github.com/slimphp/Twig-View), [Symfony Twig Bridge](https://github.com/symfony/twig-bridge))

## Required

* PHP 7.1+

## Recommended

* MySql ver 15.1

## File structure

* **bootstrap**
    * doctrine_orm.php (Doctrine entity manager launch)
    * middleware.php (App middlewares launch)
    * routes.php (routing declarations)
* **builder**
    * **dependencies** (objects injections definitions)
    * **settings** (values injections definitions)
* **config**
    * cli-config.php (Doctrine cli config)

## Installation
```php
$ git clone https://paneric@bitbucket.org/paneric/slim-4-frame.git
$ cd slim-4-frame
$ composer install
```
## Setup

* Configure data base in */builder/settings/dev(prod).php* (section "doctrine").
* Create data base (*within project folder*):
```php
$ service mysql start
$ mysql -u root -p 
MariaDB [(none)]> CREATE DATABASE slim_4_frame;
MariaDB [(none)]> QUIT;
$ vendor/bin/doctrine-migrations migrate
```
* Run PHP built-in web server:
```php
$ php -S 127.0.0.1:8888 -t public
```
* and your browser:
```php
127.0.0.1:8888/user/new
```
## Commentary

##### Doctrine EntityManager
*/public/index.php*
```php
...
    $container->set(
        EntityManager::class,
        require APP_ROOT.'/bootstrap/doctrine_orm.php'
    );
...
```
Because the application and doctrine cli config require to instantiate the entity manager for their own purposes, it has
to be accomplished apart from the container, through a separate script accessible for both of them.
It makes an instantiation of entity manager through dependency definitions - lets just say - a bit problematic. 
That is why "set" is performed.

##### RouteParser
*/public/index.php*
```php
...
    $container->set(RouteParser::class, $app->getRouteCollector()->getRouteParser());
...
```
It is normally an attribute of http request which is neither instantiated by nor injected into container, but it has to 
be accessible for Twig.

##### Controller
One of the main purposes of this skeleton is to make controllers the thinnest possible and to remove all the logic (that
that is usually coded by symfony developers within) into related services. That is why (among others) the request
object is injected into controller related service.

##### Symfony forms
Another one is to get rid of ugly separation of concepts violation and moving settings of forms options and constraints 
into separate settings file.
*/builder/settings/form_options.php*
```php
...
return [
    'user_form_type' => [
        'email' => [
            'required' => true,
            'constraints' => [
            ... 
            ]
        ],
        ...
    ],
];
```