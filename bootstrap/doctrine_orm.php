<?php

declare(strict_types=1);

use App\Builder\EntityManagerBuilder;

$entityManagerBuilder = new EntityManagerBuilder(
    (require APP_ROOT.'/builder/settings/dev.php')['doctrine']
);

return $entityManagerBuilder->build();
