<?php

declare(strict_types=1);

use App\Middleware\BaseUrlMiddleware;
use App\Middleware\UriMiddleware;

$app->addMiddleware(new BaseUrlMiddleware($app->getBasePath()));
$app->addMiddleware(new UriMiddleware($container));
$app->addRoutingMiddleware();

// always last one
$errorMiddleware = $app->addErrorMiddleware(
    true,
    true,
    true
);
