<?php

declare(strict_types=1);

use App\Controller\UserController;
use App\Middleware\RequestMiddleware;
use App\Service\HomeService;
use App\Service\UserService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use App\Controller\HomeController;

$app->get('/', function(Request $request, Response $response) {
    return $this->get(HomeController::class)->index($request, $response, $this->get(HomeService::class));
})
    ->setName('home.index');


$app->get('/user', function(Request $request, Response $response) {
    return $this->get(UserController::class)->index($response, $this->get(UserService::class));
})
    ->setName('user.index');

$app->map(['GET', 'POST'],'/user/new', function(Request $request, Response $response) {
    $formOptions = $this->get('settings')['user_form_type'];
    return $this->get(UserController::class)->new($response, $this->get(UserService::class), $formOptions);
})
    ->setName('user.new')
    ->addMiddleware(new RequestMiddleware($container));
