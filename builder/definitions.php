<?php

declare(strict_types=1);

return array_merge (
    [
        'settings' => array_merge(
            require_once ('settings/'.ENV.'.php'),
            require_once ('settings/form_options.php')
        )
    ],
    require_once ('dependencies/middleware.php'),
    require_once ('dependencies/controller.php'),
    require_once ('dependencies/service.php'),
    require_once ('dependencies/repository.php'),
    require_once ('dependencies/form.php'),
    require_once ('dependencies/package.php')
);
