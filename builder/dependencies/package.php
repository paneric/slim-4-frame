<?php

declare(strict_types=1);

use App\Builder\FormFactoryBuilder;
use DI\Container;
use Slim\Psr7\Uri;
use Slim\Routing\RouteParser;
use Slim\Views\Twig;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Form\Extension\Csrf\CsrfExtension;
use App\Builder\TwigBuilder;
use App\Builder\TranslatorBuilder;

return [
    FormFactoryInterface::class => function (): FormFactoryInterface
    {
        $formFactoryBuilder =  new FormFactoryBuilder();

        return $formFactoryBuilder->build();
    },

    Translator::class => function (Container $container): Translator
    {
        /** @noinspection MissingService */
        $translatorBuilder = new TranslatorBuilder(
            $container->get(YamlFileLoader::class),
            $container->get('settings')['translation']
        );

        return $translatorBuilder->build();
    },

    Twig::class => function (Container $container): Twig
    {
        /** @noinspection MissingService */
        $twigBuilder = new TwigBuilder(
            $container->get(CsrfTokenManager::class),
            $container->get(RouteParser::class),
            $container->get(Uri::class),
            $container->get(Translator::class),
            $container->get('settings')['twig'],
            APP_ROOT
        );

        return $twigBuilder->build();
    },

    CsrfExtension::class => function (Container $container): CsrfExtension
    {
        return new CsrfExtension(
            $container->get(CsrfTokenManager::class)
        );
    },

    FormFactory::class => function (Container $container): FormFactoryInterface
    {
        return Forms::createFormFactoryBuilder()
            ->addExtension($container->get(CsrfExtension::class))
            ->getFormFactory();

    },
];
