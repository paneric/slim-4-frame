<?php

declare(strict_types=1);

return [
    'displayErrorDetails' => true,
    'determineRouteBeforeAppMiddleware' => true,

    'doctrine' => [
        // you should add any other path containing annotated entity classes
        'paths' => [
            APP_ROOT.'/src/Entity',
        ],

        // if true, metadata caching is forcefully disabled
        'is_dev_mode' => true,

        'proxy_dir' => null,

        'cache' => null,

        'use_simple_annotation_reader' => false,

        // path where the compiled metadata info will be cached
        // make sure the path exists and it is writable
        'cache_dir' => APP_ROOT.'/var/cache/doctrine',

        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => '127.0.0.1',
            'port' => 3306,
            'dbname' => 'slim_4_frame',
            'user' => 'root',
            'password' => 'root',
            'charset' => 'UTF8'
        ]
    ],

    'twig' => [
        'cache_dir' => APP_ROOT.'/var/cache/twig',
        'templates_dir' => APP_ROOT.'/templates',
        'default_form_theme' => 'form_div_layout.html.twig',
        'vendor_dir' => APP_ROOT.'/vendor',
    ],

    'translation' => [
        'form_translations_en' => APP_ROOT.'/translations/form.en.yaml',
        'locale' => 'en_En'
    ],
];
