<?php

use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

return [

    'user_form_type' => [
        'email' => [
            'required' => true,
            'constraints' => [
                new NotBlank(['message' => 'Please enter an email',]),
                new Email(['message' => 'Please enter a valid email',]),
            ]
        ],
        'password' => [
            'required' => true,
            'constraints' => [
                new NotBlank(['message' => 'Please enter a password',]),
                new Length([
                    'min' => 6,
                    'minMessage' => 'Your password should be at least {{ limit }} characters',
                    'max' => 20,
                ])
            ]
        ],
    ],

];
