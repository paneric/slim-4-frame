<?php

declare(strict_types=1);

define('ENV', 'dev');
define('APP_ROOT', dirname(__DIR__));

require APP_ROOT.'/vendor/autoload.php';

use DI\ContainerBuilder;
use Doctrine\ORM\EntityManager;
use Slim\Factory\AppFactory;
use Slim\Routing\RouteParser;

try {
    $builder = new ContainerBuilder();
    $builder->useAutowiring(true);
    $builder->useAnnotations(true);

    $builder->addDefinitions(
        require_once APP_ROOT. '/builder/definitions.php'
    );

    $container = $builder->build();

    AppFactory::setContainer($container);
    $app = AppFactory::create();

    $container->set(
        EntityManager::class,
        require APP_ROOT.'/bootstrap/doctrine_orm.php'
    );
    $container->set('base_path', APP_ROOT);
    $container->set(RouteParser::class, $app->getRouteCollector()->getRouteParser());

    require APP_ROOT.'/bootstrap/middleware.php';

    require APP_ROOT.'/bootstrap/routes.php';

    $app->run();

} catch (Exception $e) {
    echo $e->getMessage();
}
