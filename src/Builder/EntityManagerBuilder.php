<?php

declare(strict_types=1);

namespace App\Builder;

use Doctrine\ORM\ORMException;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;

class EntityManagerBuilder
{
    private $settings;

    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    public function build(): EntityManager
    {
        try {
            $config = Setup::createAnnotationMetadataConfiguration(
                $this->settings['paths'],
                $this->settings['is_dev_mode'],
                $this->settings['proxy_dir'],
                $this->settings['cache'],
                $this->settings['use_simple_annotation_reader']
            );

            $config->setMetadataDriverImpl(
                new AnnotationDriver(
                    new AnnotationReader,
                    $this->settings['paths']
                )
            );

            $config->setMetadataCacheImpl(
                new FilesystemCache(
                    $this->settings['cache_dir']
                )
            );

            return EntityManager::create(
                $this->settings['connection'],
                $config
            );
        } catch (ORMException $e) {
            $e->getMessage();
        }

        return null;
    }
}
