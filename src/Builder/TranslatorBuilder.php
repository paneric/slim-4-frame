<?php

declare(strict_types=1);

namespace App\Builder;

use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Component\Translation\Translator;

class TranslatorBuilder
{
    private $yamlFileLoader;
    private $settings;

    public function __construct(YamlFileLoader $yamlFileLoader, array $settings)
    {
        $this->yamlFileLoader = $yamlFileLoader;
        $this->settings = $settings;
    }

    public function build(): Translator
    {
        $translator = new Translator($this->settings['locale']);

        $translator->addLoader('yaml', $this->yamlFileLoader);

        $translator->addResource(
            'yaml',
            $this->settings['form_translations_en'],
            $this->settings['locale']
        );

        return $translator;
    }
}
