<?php

declare(strict_types=1);

namespace App\Builder;

use ReflectionClass;
use Slim\Psr7\Uri;
use Slim\Routing\RouteParser;
use Slim\Views\Twig;
use Slim\Views\TwigExtension;
use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Bridge\Twig\Form\TwigRendererEngine;
use Symfony\Component\Form\FormRenderer;
use Symfony\Component\Security\Csrf\CsrfTokenManager;
use Symfony\Component\Translation\Translator;
use Twig\RuntimeLoader\FactoryRuntimeLoader;

class TwigBuilder
{
    private $csrfTokenManager;
    private $routeParser;
    private $uri;
    private $translator;
    private $settings;
    private $basePath;

    public function __construct(
        CsrfTokenManager $csrfTokenManager,
        RouteParser $routeParser,
        Uri $uri,
        Translator $translator,
        array $settings,
        string $basePath
    ) {
        $this->csrfTokenManager = $csrfTokenManager;
        $this->routeParser = $routeParser;
        $this->uri = $uri;
        $this->translator = $translator;
        $this->settings = $settings;
        $this->basePath = $basePath;
    }

    public function build(): Twig
    {
        $appVariableReflection = new ReflectionClass('\Symfony\Bridge\Twig\AppVariable');
        $vendorTwigBridgeDirectory = dirname($appVariableReflection->getFileName());

        $twig = new Twig([
            $this->settings['templates_dir'],
            $vendorTwigBridgeDirectory.'/Resources/views/Form',
        ], [
            'cache' => $this->settings['cache_dir']
        ]);

        $formEngine = new TwigRendererEngine([$this->settings['default_form_theme']], $twig->getEnvironment());

        $twig->getEnvironment()->addRuntimeLoader(new FactoryRuntimeLoader([
            FormRenderer::class => function () use ($formEngine) {
                return new FormRenderer($formEngine, $this->csrfTokenManager);
            },
        ]));

        /** @noinspection MissingService */
        $twigExtension = new TwigExtension(
            $this->routeParser,
            $this->uri,
            $this->basePath
        );
        $twig->addExtension($twigExtension);

        $twig->addExtension(new FormExtension());
        $twig->addExtension(new TranslationExtension($this->translator));

        return $twig;
    }
}
