<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\HomeService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig as View;

class HomeController
{
    private $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function index(Request $request, Response $response, HomeService $homeService)
    {
        $homeService->index($request);

        return $this->view->render($response, 'home/home.html.twig', [
            'name' => 'Dudus'
        ]);
    }
}
