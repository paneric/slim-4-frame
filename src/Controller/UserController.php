<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\UserService;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\Twig as View;

class UserController
{
    private $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function index(Response $response, UserService $userService)
    {
        $userService->index();

        return $this->view->render($response, 'home/home.html.twig', [
            'name' => 'Dudus'
        ]);
    }

    public function new(Response $response, UserService $userService, array $formOptions)
    {
        $responseParameters = $userService->new($formOptions);

        if (empty($responseParameters)) {
            return $response
                ->withHeader('Location', 'https://www.example.com')
                ->withStatus(302);
        }

        return $this->view->render(
            $response,
            'user/new.html.twig',
            $responseParameters
        );
    }
}
