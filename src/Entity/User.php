<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(
 *     name="users",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="email_idx", columns={"email"}),
 *         @ORM\UniqueConstraint(name="password_idx", columns={"password"})
 *     }
 * )
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     * @ORM\GeneratedValue(strategy="UUID")
     * @var string
     */
    private $id;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $email;
    /**
     * @ORM\Column(type="string")
     * @var string
     */
    private $password;
    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @var DateTime
     */
    private $createdAt;

    /**
     * User constructor.
     * @param UserDTO $userDTO
     * @throws Exception
     */
    public function __construct(UserDTO $userDTO)
    {
        $this->email = $userDTO->getEmail();
        $this->password = $userDTO->getPassword();

        $this->createdAt = new DateTime('NOW');
    }
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
