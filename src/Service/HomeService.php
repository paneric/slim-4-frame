<?php

declare(strict_types=1);

namespace App\Service;

use Psr\Http\Message\ServerRequestInterface as Request;

class HomeService
{
    public function index(Request $request): array
    {
        return [];
    }
}
