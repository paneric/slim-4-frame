<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Entity\UserDTO;
use App\Form\UserFormType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Exception;
use Symfony\Component\Form\FormFactoryInterface as FormFactory;
use Psr\Http\Message\ServerRequestInterface as Psr7Request;

class UserService extends Service
{
    private $request;
    private $formFactory;
    private $entityManager;
    private $userRepository;

    public function __construct(
        Psr7Request $psr7request,
        FormFactory $formFactory,
        EntityManager $entityManager,
        UserRepository $userRepository
    ) {
        $this->request = $this->convertPsr7Request($psr7request);
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function index(): array
    {
        return [];
    }

    public function new(array $formOptions): array
    {
        $userDTO = new UserDTO();
        $form = $this->formFactory->create(UserFormType::class, $userDTO, $formOptions);

        $form->handleRequest($this->request);
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user = new User($userDTO);
                $this->entityManager->persist($user);
                $this->entityManager->flush();

                return [];

            } catch (ORMException $e) {
                $e->getMessage();
            } catch (Exception $e) {
                $e->getMessage();
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }
}
